<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\YoutubeDl;
use Mockery\Exception;
use App\Video;

use Spatie;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $tagIds = \DB::table('taggables')
            ->distinct()
            ->select('tag_id')
            ->where('taggable_type', Video::class)
            ->get()
            ->pluck('tag_id');

        $tags = Spatie\Tags\Tag::whereIn('id', $tagIds)->get();


        if ($request->has('byTag')) {
            $videos = Video::withAnyTags([$request->input('byTag')])->get();
        } else {
            $videos = Video::all();

        }
        return view('video.index', compact(['videos', 'tags']));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $tagIds = \DB::table('taggables')
            ->distinct()
            ->select('tag_id')
            ->where('taggable_type', Video::class)
            ->get()
            ->pluck('tag_id');

        $tags = Spatie\Tags\Tag::whereIn('id', $tagIds)->get();

        return view('video.create', compact(['tags']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'url' => 'required|regex:/^https:\/\/www\.youtube\.com\/watch\?v=.+/i',
            'tags' => 'required'
        ]);


        // Instantly download a YouTube video (using the default settings).
        $NUM_OF_ATTEMPTS = 3;
        $attempts = 0;
        $hasException = true;

        do {

            try {
                $video = new YoutubeDl($request->input('url'), TRUE);

                $hasException = false;
            } catch (\Exception $e) {
                $attempts++;
                sleep(1);
                continue;
            }

            if ($hasException == true) {
                dump($e);
                abort(500, 'Problem getting video from youtube (sometimes happen) please go back and resubmit');
            }
            break;
        } while ($attempts < $NUM_OF_ATTEMPTS);
        $newVideo = new Video();

        $newVideo->title = $video->videoTitle;
        $newVideo->original_url = $request->input('url');

        $newVideo
            ->addMedia($video->downloadsDir . $video->video)
            ->toMediaCollection();
        if (isset($video->thumb)) {
            $newVideo
                ->addMedia($video->downloadsDir . $video->thumb)
                ->toMediaCollection();
        }


        $newVideo->save();
        $newVideo->syncTags($request->input('tags'));


        return redirect()->route('video.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $video = Video::findOrFail($id);

        return view('video.show',compact(['video']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

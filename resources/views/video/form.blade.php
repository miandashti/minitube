<div class="form-group {{ $errors->has('url') ? 'has-error' : ''}}">
    {!! Form::label('url', 'Url', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('url', null, ['class' => 'form-control','placeholder'=>'for ex. https://www.youtube.com/watch?v=X8rFs1TFrYk']) !!}
        {!! $errors->first('url', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('tags') ? 'has-error' : ''}}">
    {!! Form::label('tags', 'Tags', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <?php
        $tagsArray = [];

        foreach ($tags as $tag) {
            $tagsArray[$tag->name] = $tag->name;


        }

        ?>
        {!! Form::select('tags[]', $tagsArray,NULL, ['class' => 'form-control select2','multiple' => 'multiple']) !!}
        {!! $errors->first('tags', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Download and Store Video', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Tags</div>
                    <div class="panel-body">


                            @foreach($tags as $tag)
                            <a href="{{ route('video.index',['byTag' => $tag->name]) }}"><span class="label label-info">{{ $tag->name }}</span></a>
                            @endforeach

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Videos</div>
                    <div class="panel-body">

                        <div class="row">

                            @foreach($videos as $video)
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail">
                                    <img src="{{ $video->getMedia()[1]->getUrl() }}" alt="...">
                                    <div class="caption">
                                        <h5>{{ $video->title }}</h5>
                                        <p>
                                            Tags:
                                            @foreach($video->tags()->get() as $tag)
                                                <span class="label label-info">{{ $tag->name }}</span>
                                                @endforeach
                                        </p>
                                        <p><a href="{{ route('video.show',['id' => $video->id]) }}" class="btn btn-primary btn-block" role="button">Watch</a> </p>
                                    </div>
                                </div>
                            </div>
                                @endforeach
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">


            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Watch Video : {{ $video->title }}</div>
                    <div class="panel-body">

                        <video
                                id="my-player"
                                class="video-js"
                                controls
                                preload="auto"
                                @if(isset($video->getMedia()[1]))
                                poster="{{ $video->getMedia()[1]->getUrl() }}"
                                @endif
                                data-setup='{}'>


                            <source src="{{ $video->getMedia()[0]->getUrl() }}" type="{{ $video->getMedia()[0]->mime_type }}"></source>
                            <p class="vjs-no-js">
                                To view this video please enable JavaScript, and consider upgrading to a
                                web browser that
                                <a href="http://videojs.com/html5-video-support/" target="_blank">
                                    supports HTML5 video
                                </a>
                            </p>
                        </video>
                        <table class="table table-bordered">
                            <tr>
                                <td>Original</td>
                                <td>{{ $video->original_url }}</td>
                            </tr>
                            <tr>
                                <td>Tags</td>
                                <td> @foreach($video->tags()->get() as $tag)
                                        <span class="label label-info">{{ $tag->name }}</span>
                                    @endforeach</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

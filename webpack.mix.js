const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .styles(['node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker3.standalone.css'], 'public/css/all.css')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.scripts([
    'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'
], 'public/js/lib/datepicker.js');

